#!/bin/bash

root="/os"

mkdir -vp "$root/alpm-hooks/usr/share/libalpm/hooks"
find /usr/share/libalpm/hooks -exec ln -sf /dev/null "$root/alpm-hooks{}" \;
mkdir -vp "$root/var/lib/pacman/"
install -Dm644 /usr/share/devtools/pacman-extra.conf "$root/etc/pacman.conf"

# Install the required packages
fakechroot -- fakeroot -- pacman -Sy -r "$root" --noconfirm \
    --dbpath "$root/var/lib/pacman" \
    --config "/etc/pacman.conf" \
    --noscriptlet \
    base linux linux-firmware mkinitcpio-nfs-utils nfs-utils
# add -c later

# Fix the initramfs for NFSv4
sed s/nfsmount/mount.nfs4/ "$root/usr/lib/initcpio/hooks/net" > "$root/usr/lib/initcpio/hooks/netnfs4"
cp $root/usr/lib/initcpio/install/net{,nfs4}

fakechroot -- fakeroot -- arch-chroot "$root" mkinitcpio -p linux