#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="clustbootos"
iso_label="CBOS_$(date +%Y%m)"
iso_publisher="Cluster Boot OS <https://gisch.dev/>"
iso_application="Cluster Boot OS"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
file_permissions=(
  ["/etc/shadow"]="0:0:400"
)
